#!/bin/bash
. /home/engineering/member_profile/config.properties
ods_inserted_ts=$(TZ='America/Los_Angeles' date +'%Y-%m-%d %H:%M:%S.%6N-07:00')
echo $ods_inserted_ts
ods_inserted_ts_utc=$(TZ='Etc/UTC' date +'%Y-%m-%d %H:%M:%S.%6N+00:00')
echo $ods_inserted_ts_utc

sqoop job --create users_users -- import -m 1  --connect "$db_admin_url" --username postgres --password postgres123 --query "select '$ods_inserted_ts'::text,'$ods_inserted_ts_utc'::text as utc,user_id,user_email,user_password,user_active,user_email_valid,user_creation_date,user_last_modified,user_registration_type,user_partner_token,password_status_id,toolbar_status,gender,referral_token,ge_credit_card_pa_acceptance_code,ge_credit_card_pa_product_type,ge_credit_card_pa_expiration_ts,ge_credit_card_pa_approval_status,ge_credit_card_pa_approval_ts,referrer_type_id,referral_shorturl,password,locale,has_short_url,ge_credit_card_pa_start_ts  from users_users where \$CONDITIONS" --incremental append -check-column user_id --fields-terminated-by "\t" --hive-delims-replacement "anything"  --null-string "\\\\N" --null-non-string "\\\\N"   --hive-table payments --target-dir $hdfs_base_dir/temp/dealwallet.com/public_new_users_users -- --schema public
