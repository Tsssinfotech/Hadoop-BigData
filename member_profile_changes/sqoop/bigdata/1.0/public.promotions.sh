#!/bin/bash
. /home/engineering/member_profile/config.properties
ods_inserted_ts=$(TZ='America/Los_Angeles' date +'%Y-%m-%d %H:%M:%S.%6N-07:00')
echo $ods_inserted_ts
ods_inserted_ts_utc=$(TZ='Etc/UTC' date +'%Y-%m-%d %H:%M:%S.%6N+00:00')
echo $ods_inserted_ts_utc

sqoop job --create promotions -- import -m 1  --connect "$db_admin_url" --username postgres --password postgres123  --query "select'$ods_inserted_ts'::text,'$ods_inserted_ts_utc'::text as utc,up_id,up_user_id,up_amount,up_approved_date,up_payment_id,up_state,up_promo_type,up_promo_id,up_qualifying_order_id,up_creation_date,up_last_modified_date,friend_member_id,last_processed_ts,up_referee_name,modified_by from promotions where  \$CONDITIONS"  --incremental append -check-column up_id --fields-terminated-by "\t" --hive-delims-replacement "anything"  --null-string "\\\\N" --null-non-string "\\\\N"   --hive-table promotions --target-dir $hdfs_base_dir/temp/dealwallet.com/public_new_promotions -- --schema public
