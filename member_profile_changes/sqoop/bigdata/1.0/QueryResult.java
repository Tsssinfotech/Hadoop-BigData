// ORM class for table 'null'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Fri Feb 03 01:42:14 PST 2017
// For connector: org.apache.sqoop.manager.DirectPostgresqlManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class QueryResult extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  public static interface FieldSetterCommand {    void setField(Object value);  }  protected ResultSet __cur_result_set;
  private Map<String, FieldSetterCommand> setters = new HashMap<String, FieldSetterCommand>();
  private void init0() {
    setters.put("ods_inserted_ts", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        ods_inserted_ts = (java.sql.Timestamp)value;
      }
    });
    setters.put("ods_inserted_ts_utc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        ods_inserted_ts_utc = (java.sql.Timestamp)value;
      }
    });
    setters.put("id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        id = (Long)value;
      }
    });
    setters.put("user_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        user_id = (Long)value;
      }
    });
    setters.put("hold_payment", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        hold_payment = (String)value;
      }
    });
    setters.put("modified_ts", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        modified_ts = (String)value;
      }
    });
  }
  public QueryResult() {
    init0();
  }
  private java.sql.Timestamp ods_inserted_ts;
  public java.sql.Timestamp get_ods_inserted_ts() {
    return ods_inserted_ts;
  }
  public void set_ods_inserted_ts(java.sql.Timestamp ods_inserted_ts) {
    this.ods_inserted_ts = ods_inserted_ts;
  }
  public QueryResult with_ods_inserted_ts(java.sql.Timestamp ods_inserted_ts) {
    this.ods_inserted_ts = ods_inserted_ts;
    return this;
  }
  private java.sql.Timestamp ods_inserted_ts_utc;
  public java.sql.Timestamp get_ods_inserted_ts_utc() {
    return ods_inserted_ts_utc;
  }
  public void set_ods_inserted_ts_utc(java.sql.Timestamp ods_inserted_ts_utc) {
    this.ods_inserted_ts_utc = ods_inserted_ts_utc;
  }
  public QueryResult with_ods_inserted_ts_utc(java.sql.Timestamp ods_inserted_ts_utc) {
    this.ods_inserted_ts_utc = ods_inserted_ts_utc;
    return this;
  }
  private Long id;
  public Long get_id() {
    return id;
  }
  public void set_id(Long id) {
    this.id = id;
  }
  public QueryResult with_id(Long id) {
    this.id = id;
    return this;
  }
  private Long user_id;
  public Long get_user_id() {
    return user_id;
  }
  public void set_user_id(Long user_id) {
    this.user_id = user_id;
  }
  public QueryResult with_user_id(Long user_id) {
    this.user_id = user_id;
    return this;
  }
  private String hold_payment;
  public String get_hold_payment() {
    return hold_payment;
  }
  public void set_hold_payment(String hold_payment) {
    this.hold_payment = hold_payment;
  }
  public QueryResult with_hold_payment(String hold_payment) {
    this.hold_payment = hold_payment;
    return this;
  }
  private String modified_ts;
  public String get_modified_ts() {
    return modified_ts;
  }
  public void set_modified_ts(String modified_ts) {
    this.modified_ts = modified_ts;
  }
  public QueryResult with_modified_ts(String modified_ts) {
    this.modified_ts = modified_ts;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.ods_inserted_ts == null ? that.ods_inserted_ts == null : this.ods_inserted_ts.equals(that.ods_inserted_ts));
    equal = equal && (this.ods_inserted_ts_utc == null ? that.ods_inserted_ts_utc == null : this.ods_inserted_ts_utc.equals(that.ods_inserted_ts_utc));
    equal = equal && (this.id == null ? that.id == null : this.id.equals(that.id));
    equal = equal && (this.user_id == null ? that.user_id == null : this.user_id.equals(that.user_id));
    equal = equal && (this.hold_payment == null ? that.hold_payment == null : this.hold_payment.equals(that.hold_payment));
    equal = equal && (this.modified_ts == null ? that.modified_ts == null : this.modified_ts.equals(that.modified_ts));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.ods_inserted_ts == null ? that.ods_inserted_ts == null : this.ods_inserted_ts.equals(that.ods_inserted_ts));
    equal = equal && (this.ods_inserted_ts_utc == null ? that.ods_inserted_ts_utc == null : this.ods_inserted_ts_utc.equals(that.ods_inserted_ts_utc));
    equal = equal && (this.id == null ? that.id == null : this.id.equals(that.id));
    equal = equal && (this.user_id == null ? that.user_id == null : this.user_id.equals(that.user_id));
    equal = equal && (this.hold_payment == null ? that.hold_payment == null : this.hold_payment.equals(that.hold_payment));
    equal = equal && (this.modified_ts == null ? that.modified_ts == null : this.modified_ts.equals(that.modified_ts));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.ods_inserted_ts = JdbcWritableBridge.readTimestamp(1, __dbResults);
    this.ods_inserted_ts_utc = JdbcWritableBridge.readTimestamp(2, __dbResults);
    this.id = JdbcWritableBridge.readLong(3, __dbResults);
    this.user_id = JdbcWritableBridge.readLong(4, __dbResults);
    this.hold_payment = JdbcWritableBridge.readString(5, __dbResults);
    this.modified_ts = JdbcWritableBridge.readString(6, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.ods_inserted_ts = JdbcWritableBridge.readTimestamp(1, __dbResults);
    this.ods_inserted_ts_utc = JdbcWritableBridge.readTimestamp(2, __dbResults);
    this.id = JdbcWritableBridge.readLong(3, __dbResults);
    this.user_id = JdbcWritableBridge.readLong(4, __dbResults);
    this.hold_payment = JdbcWritableBridge.readString(5, __dbResults);
    this.modified_ts = JdbcWritableBridge.readString(6, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeTimestamp(ods_inserted_ts, 1 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ods_inserted_ts_utc, 2 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeLong(id, 3 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeLong(user_id, 4 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(hold_payment, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(modified_ts, 6 + __off, 12, __dbStmt);
    return 6;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeTimestamp(ods_inserted_ts, 1 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ods_inserted_ts_utc, 2 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeLong(id, 3 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeLong(user_id, 4 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(hold_payment, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(modified_ts, 6 + __off, 12, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.ods_inserted_ts = null;
    } else {
    this.ods_inserted_ts = new Timestamp(__dataIn.readLong());
    this.ods_inserted_ts.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ods_inserted_ts_utc = null;
    } else {
    this.ods_inserted_ts_utc = new Timestamp(__dataIn.readLong());
    this.ods_inserted_ts_utc.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.id = null;
    } else {
    this.id = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.user_id = null;
    } else {
    this.user_id = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.hold_payment = null;
    } else {
    this.hold_payment = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.modified_ts = null;
    } else {
    this.modified_ts = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.ods_inserted_ts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ods_inserted_ts.getTime());
    __dataOut.writeInt(this.ods_inserted_ts.getNanos());
    }
    if (null == this.ods_inserted_ts_utc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ods_inserted_ts_utc.getTime());
    __dataOut.writeInt(this.ods_inserted_ts_utc.getNanos());
    }
    if (null == this.id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.id);
    }
    if (null == this.user_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.user_id);
    }
    if (null == this.hold_payment) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, hold_payment);
    }
    if (null == this.modified_ts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, modified_ts);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.ods_inserted_ts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ods_inserted_ts.getTime());
    __dataOut.writeInt(this.ods_inserted_ts.getNanos());
    }
    if (null == this.ods_inserted_ts_utc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ods_inserted_ts_utc.getTime());
    __dataOut.writeInt(this.ods_inserted_ts_utc.getNanos());
    }
    if (null == this.id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.id);
    }
    if (null == this.user_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.user_id);
    }
    if (null == this.hold_payment) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, hold_payment);
    }
    if (null == this.modified_ts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, modified_ts);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 9, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(ods_inserted_ts==null?"\\N":"" + ods_inserted_ts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ods_inserted_ts_utc==null?"\\N":"" + ods_inserted_ts_utc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(id==null?"\\N":"" + id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(user_id==null?"\\N":"" + user_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(hold_payment==null?"\\N":hold_payment, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(modified_ts==null?"\\N":modified_ts, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(ods_inserted_ts==null?"\\N":"" + ods_inserted_ts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ods_inserted_ts_utc==null?"\\N":"" + ods_inserted_ts_utc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(id==null?"\\N":"" + id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(user_id==null?"\\N":"" + user_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(hold_payment==null?"\\N":hold_payment, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(modified_ts==null?"\\N":modified_ts, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 9, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ods_inserted_ts = null; } else {
      this.ods_inserted_ts = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ods_inserted_ts_utc = null; } else {
      this.ods_inserted_ts_utc = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.id = null; } else {
      this.id = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.user_id = null; } else {
      this.user_id = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.hold_payment = null; } else {
      this.hold_payment = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.modified_ts = null; } else {
      this.modified_ts = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ods_inserted_ts = null; } else {
      this.ods_inserted_ts = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ods_inserted_ts_utc = null; } else {
      this.ods_inserted_ts_utc = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.id = null; } else {
      this.id = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.user_id = null; } else {
      this.user_id = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.hold_payment = null; } else {
      this.hold_payment = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.modified_ts = null; } else {
      this.modified_ts = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    QueryResult o = (QueryResult) super.clone();
    o.ods_inserted_ts = (o.ods_inserted_ts != null) ? (java.sql.Timestamp) o.ods_inserted_ts.clone() : null;
    o.ods_inserted_ts_utc = (o.ods_inserted_ts_utc != null) ? (java.sql.Timestamp) o.ods_inserted_ts_utc.clone() : null;
    return o;
  }

  public void clone0(QueryResult o) throws CloneNotSupportedException {
    o.ods_inserted_ts = (o.ods_inserted_ts != null) ? (java.sql.Timestamp) o.ods_inserted_ts.clone() : null;
    o.ods_inserted_ts_utc = (o.ods_inserted_ts_utc != null) ? (java.sql.Timestamp) o.ods_inserted_ts_utc.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new HashMap<String, Object>();
    __sqoop$field_map.put("ods_inserted_ts", this.ods_inserted_ts);
    __sqoop$field_map.put("ods_inserted_ts_utc", this.ods_inserted_ts_utc);
    __sqoop$field_map.put("id", this.id);
    __sqoop$field_map.put("user_id", this.user_id);
    __sqoop$field_map.put("hold_payment", this.hold_payment);
    __sqoop$field_map.put("modified_ts", this.modified_ts);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("ods_inserted_ts", this.ods_inserted_ts);
    __sqoop$field_map.put("ods_inserted_ts_utc", this.ods_inserted_ts_utc);
    __sqoop$field_map.put("id", this.id);
    __sqoop$field_map.put("user_id", this.user_id);
    __sqoop$field_map.put("hold_payment", this.hold_payment);
    __sqoop$field_map.put("modified_ts", this.modified_ts);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if (!setters.containsKey(__fieldName)) {
      throw new RuntimeException("No such field:"+__fieldName);
    }
    setters.get(__fieldName).setField(__fieldVal);
  }

}
