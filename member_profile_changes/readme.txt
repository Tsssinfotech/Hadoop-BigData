===================
Member Profile Job
===================

  The member profile job is responsible for processing the data generated in dealwallet/IRS applications and store it into DW HDFS location in the form of parquet files. The member profile data consists of following tables from Dealwallet/IRS:

1. users
2. users_info
3. payments
4. user_hold_payments
5. shopping trips
6. promotions
7. visits

The whole process was devided into 3 different jobs.

Job1: job1 is responsible for moving the data from postgres to ODS files by using sqoop.
=====

Job2: job2 is responsible for moving data from events logs to dw through ods and di 
=====

Job3: job3 is responsible to aggreagate the data from ODS to DW location in the form of parquet files
=====

The automation scripts created were responsible for 

  1. creating the tables in postgres DB (provided in the configuration) 
  2. generating the valid sample data into these tables
  3. running the sqoop jobs for moving this data into ODS files
  4. generating visits data from events logs to dw through ods and di
  5. running the member_profile spark job to process the data and store it into DW files
  6. running the python scripts for applying various validations including data validation, schema validation, partition validation and etc..
  7. sending the email and hipchat with the status of the job execution

Please follow below step by step execution to understand the whole automation process and replicating the error scenarios and verifying the behaviour of scripts in the error cases:

Step 1: copy the zip file into the bigdata server and extract it to following location "/home/engineering/"

Step 2: 

  once extracted, open the following file "/home/engineering/member_profile_changes/job1_auto_test.sh" and look for the following line

  "python /home/engineering/member_profile_changes/table_creation_pythonscripts/create_data.py <data_sequence_start_id> <no_of_record_to_insert_ineach_table> <name_of_database> <db_username> <db_password> <db_host> <db_port>"

Please update the correct database details like db_name, username, password, host and port

update the email address to your email in the following file "/home/engineering/member_profile_changes/config.properties" => "To" field

update hdfs host and port in member_profile_changes/passingArguments/member.py file at

#Configure the host name of the hdfs file system
hdfs = HDFileSystem(host='host_name', port=port_num)

create path in hdfs browser like '/user/hue/ods/etl/' and keep all the files containing in the folder 'member_profile_changes/etl'.
create path in hdfs browser like '/user/eventlog/dealwallet.com/archive_201703/' and keep the files from 'member_profile_changes/dealwallet.com/archive_201703' folder to that location.

create path in hdfs browser like 'user/hue/ods/dealwalletcom' and keep 2 folders named "webanalytics_dealwallet_external_entry" and "webanalytics_dealwallet_external_entry_category" in that location from "member_profile_changes" folder.

create path in hdfs browser like '/user/hue/di/visits/tenant=dealwallet.com/load_type=current' and "/user/hue/dw/visits/tenant=dealwallet.com" before running the script  

Step 3:

  Now, execute the following command "./home/engineering/member_profile_changes/member_job.sh member_profile-1.0-SNAPSHOT.jar". Here it will execute all the table creation, data generation, sqoop jobs, spark job to processthe data, running automation script for validating various test case. at the end of this job you will get an email and hipchat with job status and log file as attachement. In this case all went well as it is a positive test case scenario.

Step 4:

  Now, execute the following command "./home/engineering/member_profile_changes/member_job.sh member_profile_sorting_error-1.0-SNAPSHOT.jar". The automation script will be failed with validation error and you will get an email and hipchat with error details.

  
Note: member_profile_sorting_error-1.0-SNAPSHOT.jar is created with sorting error to simulating the error in the code and observe the behaviour of the automation script

Step 5:

  Now, execute the following command "./home/engineering/member_profile_changes/member_job.sh member_profile_partiton_error-1.0-SNAPSHOT.jar". The automation script will be failed with validation error and you will get the email and hipchat with error details.

Note: member_profile_partiton_error-1.0-SNAPSHOT.jar is created with partition error to simulate the error in the code and observe the behaviour of the automation script


Following are the test cases covered in the python scripts (step 4 and 5 are to generate errors to validate the scripts):

1. Data validation between generated DW files with target DW files (any mismatches in the data will fail the test case)
2. File size comparision check. (the generated file must be in a predefined range otherwise the test case will be failed)
3. Schema validation
4. Sorting order in each parquet file
5. Validating the no of partitions
6. ODS data comparision check

Future enhancements planned:

1. We are going to work on the reporting tools for generating test case execution reports that can be sent via emails
2. adding more possible test cases
