import psycopg2
import ConfigParser
import os
import sys
#Start_value= int(raw_input("enter number:"))
#End_value= int(raw_input("enter number:"))
#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <start_value> <end_value> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)

# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, start_value, end_value,database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, start_value, end_value, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, start_value, end_value, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv
originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]
config = ConfigParser.RawConfigParser()
config.read('/home/tsssinfotech/Desktop/tables_creation_python/tablevalues.properties')

#conn = psycopg2.connect(database="bigdata",user="dealwallet",password="infotech369",host="localhost",port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened database successfully"

cur = conn.cursor()
# drop table visits
cur.execute('''drop table visits;''')
conn.commit()
print "drop visits table successfully"

#CREATE A TABLE dw.visits

cur.execute('''create table if not exists visits (dw_inserted_ts timestamp without time zone,dw_inserted_ts_utc timestamp without time zone,ods_inserted_ts timestamp without time zone,ods_inserted_ts_utc timestamp without time zone,event_id bigint,event_date timestamp without time zone,event_date_id bigint,event_time timestamp without time zone,user_id bigint,non_member_id bigint,hash_non_member_id bigint,ip_address character varying,event_type character varying,request_id bigint,session_id bigint,hash_session_id bigint,entity_id bigint,visit_id bigint,referrer_id bigint,request_url_stem character varying,request_url_query character varying,browseragent character varying,browser_lang character varying,referring_query character varying,referring_url character varying,is_signup character varying,is_login character varying,landing_url_domain character varying,referring_url_domain character varying,ip_location_id character varying,country_short character varying,country_long character varying,region character varying,city character varying,zipcode character varying,os_browser_id character varying,device_family character varying,os_family character varying,os_version_int character varying,browser_family character varying,browser_version_int character varying,ua_parser_bot_flag character varying,is_mobile bigint,is_tablet character varying,is_pc character varying,traffic_source_id character varying,traffic_source character varying,traffic_source_type character varying,traffic_source_subtype character varying,application_id bigint,application_type character varying,application_subtype character varying,eeid bigint,src character varying,acct character varying,camp character varying,utype character varying,product character varying,gglnum character varying,referrerid character varying,kw character varying,adg character varying,mt character varying,query character varying,sourcename character varying,source character varying,source_id bigint,search_keyword_id character varying,event_db_created_ts timestamp without time zone,visit_db_created_ts timestamp without time zone,referrer_db_created_ts timestamp without time zone,is_fast_click_bit character varying,is_user_agent_bot character varying,device_platform character varying,cbsp_session_id bigint,utm_medium character varying,utm_campaign character varying,utm_source character varying,utm_content character varying,utm_term character varying,utm_size character varying,utm_pub character varying,utm_ebs character varying,url_format_2016 character varying);''')
conn.commit()
print "visits table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING THE VALUES INTO THE POSTGRES TABLE visits" 
#myprops = dict(line.strip().split('=') for line in open('/home/tsssinfotech/Desktop/tables_creation_python/tablevalues.properties'))

for i in range (int(start_value),int(end_value)):
   cur.execute('''insert into visits (dw_inserted_ts,dw_inserted_ts_utc,event_id,event_date,event_date_id,event_time,user_id,non_member_id,hash_non_member_id,ip_address,event_type,request_id,session_id,hash_session_id,entity_id,visit_id,referrer_id,request_url_stem,request_url_query,browseragent,browser_lang,referring_query,referring_url,is_signup,is_login,landing_url_domain,referring_url_domain,ip_location_id,country_short,country_long,region,city,zipcode,os_browser_id,device_family,os_family,os_version_int,browser_family,browser_version_int,ua_parser_bot_flag,is_mobile,is_tablet,is_pc,traffic_source_id,traffic_source,traffic_source_type,traffic_source_subtype,application_id,application_type,application_subtype,eeid,src,acct,camp,utype,product,gglnum,referrerid,kw,adg,mt,query,sourcename,source,source_id,search_keyword_id,event_db_created_ts,visit_db_created_ts,referrer_db_created_ts,is_fast_click_bit,is_user_agent_bot,device_platform,cbsp_session_id,utm_medium,utm_campaign,utm_source,utm_content,utm_term,utm_size,utm_pub,utm_ebs,url_format_2016) values ('2015-04-18 13:58:13.870','2015-04-18 13:58:13.870','%s','2013-01-31 09:58:00.000','2013%s','2013-01-31 09:58:00.000','%s','50%s','547129%s','50.54.135.45','2','1','45','-7615095184745351354','85','382037923','1','http://www.gmail.com/index.jsp\N','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)','NULL','NULL','NULL','0','1','gmail.com','US_UNITEDSTATES_WASHINGTON_MT.VERNON_98%s','INDIA','ANDHRAPRADESH','WASHINGTON','MT.ram%s... 9.0','False','NULL','NULL','NULL','Direct_Direct_Direct','Direct','Direct','Direct','Website_Website','123','23','NULL','NULL','NULL','NULL','NULL','','24','NULL','NULL','10','NULL','Unknown','1','NULL','2015-04-18 13:58:13.870','2015-04-18 13:58:13.870','2015-04-18 13:58:13.870','false','false','Desktop','NULL','NULL','NULL','10','NULL','2015-04-18 13:58:13.870','2015-04-18 13:58:13.870','2015-04-18 13:58:13.870','NULL','NULL','NULL','10','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL')'''%(i,i,i,i,i,i,i))
   conn.commit()
print "values successfully inserted"

