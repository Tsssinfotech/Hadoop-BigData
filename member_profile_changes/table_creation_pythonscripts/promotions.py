import psycopg2
import sys
import os

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################

if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

#conn = psycopg2.connect(database="bigdata", user="dealwallet", password="infotech369", host="localhost", port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened bigdata database successfully"

cur = conn.cursor()

# drop promotions table
cur.execute('''drop table promotions;''')
conn.commit()
print "successfully drop promotions table"

#CREATE A TABLE promotions

cur.execute('''create table if not exists promotions (up_id numeric,up_user_id numeric,up_amount numeric,up_approved_date timestamp without time zone,up_payment_id numeric,up_state smallint,up_promo_type smallint,up_promo_id numeric,up_qualifying_order_id numeric,up_creation_date timestamp without time zone,up_last_modified_date timestamp without time zone,friend_member_id bigint,last_processed_ts timestamp without time zone,up_referee_name character varying(256),modified_by integer);''')
conn.commit()
print "promotions table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE promotions" 
#myprops = dict(line.strip().split('=') for line in open('/home/tsssinfotech/Desktop/tables_creation_python/tablevalues.properties'))

for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*180000
      for i in [x]:
         cur.execute('''insert into promotions (up_id,up_user_id,up_amount,up_approved_date,up_payment_id,up_state,up_promo_type,up_promo_id,up_qualifying_order_id,up_creation_date,up_last_modified_date,friend_member_id,last_processed_ts,up_referee_name,modified_by) values (%s,%s,10%s,current_date-13,0,'1','1',6%s,'0',current_date-25,current_date-22,'2',current_date,'','2')'''%(i,i,i,i))
         conn.commit()
print "values successfully inserted"


#TRUNCATE THE TABLE COMMAND
#cur.execute('''truncate table promotions;''')
#conn.commit()
#print "successfully truncated table promotions"


#DROP THE TABLE promotions
#cur.execute('''drop table promotions;''')
#conn.commit()
#print "successfully drop the table promotions"


