import psycopg2
#!/usr/bin/python
import sys
import os

#Start_value= int(raw_input("enter number:"))
#End_value= int(raw_input("enter number:"))
#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

#conn = psycopg2.connect(database="bigdata", user="dealwallet", password="infotech369", host="localhost", port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened database successfully"

cur = conn.cursor()
#drop table user_hold_payment
cur.execute('''drop table users_user_hold_payment''')
conn.commit
print "successfully drop table users_user_hold_payment"
#CREATE A TABLE USERS_INFO

cur.execute('''CREATE TABLE IF NOT EXISTS users_user_hold_payment(id integer,user_id numeric,hold_payment boolean,modified_ts timestamp with time zone);''')
conn.commit()
print "users_user_hold_payment table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING THE VALUES INTO THE POSTGRES TABLE users_user_hold_payment" 

for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*180000
      for i in [x]:
         cur.execute('''insert into users_user_hold_payment (id,user_id,hold_payment,modified_ts) values (%s,%s,'False','2015-01-31 20:57:41.922000-08:00')'''%(i,i))
         conn.commit()
print "values successfully inserted"
