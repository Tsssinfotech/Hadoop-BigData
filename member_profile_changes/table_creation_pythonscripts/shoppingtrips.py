import psycopg2
import os
import sys

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################

if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

#conn = psycopg2.connect(database="bigdata",user="dealwallet",password="infotech369",host="localhost",port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened database bigdata successfully"

cur = conn.cursor()

# drop table shoppingtrips
cur.execute(''' drop table if exists shoppingtrips;''')
conn.commit()
print "successfully drop table shoppingtrips"

#CREATE A TABLE USERS_INFO

cur.execute('''CREATE TABLE IF NOT EXISTS shoppingtrips (dw_inserted_ts timestamp with time zone,dw_inserted_ts_utc timestamp with time zone,id bigint,reward_id bigint,store_id bigint,member_id bigint,click_date character varying,tt_date_id character varying,click_time character varying,entity_id bigint,entity_type_id character varying,coupon_id character varying,campaign_id character varying,tt_created_ts character varying,tt_modified_ts character varying,tt_db_created_ts character varying,tt_db_modified_ts character varying,tracking_ticket_number character varying,tt_source_id character varying,tt_source_name character varying,group_entity_id character varying,group_entity_type_id character varying,tracking_uri character varying,shopping_uri character varying,non_cashback_member_id character varying,note character varying,nav_id character varying,ee_id character varying,mpl_id character varying,referral_nav_id character varying,mst_db_created_ts character varying,mst_ods_inserted_ts character varying,session_id character varying,event_id character varying,non_member_id character varying,hash_non_member_id character varying,ip_address character varying,visit_session_id character varying,hash_session_id character varying,visit_id character varying,referrer_id character varying,request_url_stem character varying,request_url_query character varying,browseragent character varying,browser_lang character varying,referring_query character varying,referring_url character varying,is_signup character varying,is_login character varying,landing_url_domain character varying,referring_url_domain character varying,ip_location_id character varying,country_short character varying,country_long character varying,region character varying,city character varying,zipcode character varying,os_browser_id character varying,device_family character varying,os_family character varying,os_version_int character varying,browser_family character varying,browser_version_int character varying,ua_parser_bot_flag character varying,is_mobile character varying,is_tablet character varying,is_pc character varying,traffic_source_id character varying,traffic_source character varying,traffic_source_type character varying,traffic_source_subtype character varying,application_id character varying,application_type character varying,application_subtype character varying,visit_src_param character varying,visit_acct_param character varying,visit_campaign_param character varying,visit_utype_param character varying,visit_product_param character varying,visit_gglnum_param character varying,visit_referrerid_param character varying,visit_kw_param character varying,visit_adg_param character varying,visit_mt_param character varying,visit_query_param character varying,is_fast_click_bit character varying,is_user_agent_bot character varying,merchant_rebate character varying,device_platform character varying,parent_campaign_id character varying,cbsp_session_id character varying,is_cashback character varying,tracking_uri_domain character varying,utm_medium character varying,utm_campaign character varying,utm_source character varying,utm_content character varying,utm_term character varying,utm_size character varying,utm_pub character varying,utm_ebs character varying,url_format_2016 character varying,mobile_app_session_id character varying,mobile_app_application_type character varying,mobile_app_application_subtype character varying,mobile_device_platform character varying,mobile_app_version character varying,mobile_app_os_version character varying,mobile_app_eeid character varying,apptimize_data character varying,mobile_app_device_ids character varying,mobile_app_android_id character varying);''')
print "shoppingtrips table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING THE VALUES INTO THE POSTGRES TABLE shoppingtrips" 

for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*180000
      for i in [x]:
         cur.execute('''insert into shoppingtrips(dw_inserted_ts,dw_inserted_ts_utc,id,reward_id,store_id,member_id,click_date,tt_date_id,click_time,entity_id,entity_type_id,  coupon_id,campaign_id,tt_created_ts,tt_modified_ts,tt_db_created_ts,tt_db_modified_ts,tracking_ticket_number,tt_source_id,tt_source_name,group_entity_id, group_entity_type_id,tracking_uri,shopping_uri,non_cashback_member_id,note,nav_id,ee_id,mpl_id,referral_nav_id,mst_db_created_ts,mst_ods_inserted_ts,session_id, event_id,non_member_id,hash_non_member_id,ip_address,visit_session_id,hash_session_id,visit_id,referrer_id,request_url_stem,request_url_query,browseragent, browser_lang,referring_query,referring_url,is_signup,is_login,landing_url_domain,referring_url_domain,ip_location_id,country_short,country_long,region,city, zipcode,os_browser_id,device_family,os_family,os_version_int,browser_family,browser_version_int,ua_parser_bot_flag,is_mobile,is_tablet,is_pc,traffic_source_id, traffic_source,traffic_source_type,traffic_source_subtype,application_id,application_type,application_subtype,visit_src_param,visit_acct_param,visit_campaign_param, visit_utype_param,visit_product_param,visit_gglnum_param,visit_referrerid_param,visit_kw_param,visit_adg_param,visit_mt_param,visit_query_param,is_fast_click_bit, is_user_agent_bot,merchant_rebate,device_platform,parent_campaign_id,cbsp_session_id,is_cashback,tracking_uri_domain,utm_medium,utm_campaign,utm_source,utm_content, utm_term,utm_size,utm_pub,utm_ebs,url_format_2016,mobile_app_session_id,mobile_app_application_type,mobile_app_application_subtype,mobile_device_platform, mobile_app_version,mobile_app_os_version,mobile_app_eeid,apptimize_data,mobile_app_device_ids,mobile_app_android_id) values ('2016-10-13 04:38','2016-10-13 11:38:42.548593+00:00',%s,12%s,82%s,'%s','09/10/16 14:34','20161009','14:34:12',82%s,'1','None','None','09/10/16 14:34','09/10/16 14:34','09/10/16 14:34','09/10/16 14:34','1037355%s','22392','Android-Phone','\N','\N','http://click.linksynergy.com/fs-bin/click?id=AysPbYF8vuM&offerid=277852.10000533&subid=0&type=4&u1=%s','/shopping/ticket/mall_ctrl.do?merchant_id=8257','\N','\N','None','None','None','None','None','None','aaaC1xjo5JrObcT%s','None','None','None','None','None','None','None','None','None','None','None','None','None','None','0','0','None','None','N/A','N/A','N/A','N/A','N/A','N/A','Android-Phone_Android_N/A_Android_N/A_False_True_False_False','Android-Phone','Android','N/A','Android','N/A','FALSE','TRUE','FALSE','FALSE','MobileApp_MobileApp_AndroidPhoneApp','Mobile App','Mobile App','Android Phone App','App_Android_Phone','App','Android Phone','None','None','None','None','None','None','None','None','None','None','None','FALSE','FALSE','0','Smartphone','-1','\N','-1','\N','None','None','None','none','none','none','none','none','none','none','none','none','none','None','None','None','None','None','None')'''%(i,i,i,i,i,i,i,i))
         conn.commit()
print "values successfully inserted into shoppingtrips"














