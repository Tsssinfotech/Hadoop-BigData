#importing the required packages
import os
import sys
import psycopg2
import time
import contextlib
import ConfigParser

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   partition_range = config.get('Properties','partition_range')

####################################################################################################################################
@contextlib.contextmanager
def timer(name="duration"):
    'Utility function for timing execution'
    start=time.time()
    yield
    duration=time.time()-start
    print "{0}: {1} second(s)".format(name,duration)

conn= psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened bigdata database successfully"
cur = conn.cursor()
#DROP THE TABLE users_info
cur.execute('''drop table users_info;''')
conn.commit()
print "successfully drop the table users_info"
#CREATE A TABLE users_info

cur.execute('''create table if not exists users_info(ui_id numeric,ui_user_id numeric,ui_firstname character varying,ui_lastname character varying,ui_address1 character varying,ui_address2 character varying,ui_postcode character varying,ui_country_id integer,ui_city character varying,ui_state_id character varying,ui_province character varying,ui_last_modified timestamp without time zone,ui_barcode character varying,ui_record_type character varying,source_ip_address inet,source_country text,browser_language text);''')
conn.commit()
print "users_info table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE users_info"
for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*int(partition_range)
      for i in [x]:
             cur.execute('''INSERT INTO users_info(ui_id,ui_user_id,ui_firstname,ui_lastname,ui_address1,ui_address2,ui_postcode,ui_country_id,ui_city,ui_state_id,ui_province,ui_last_modified,ui_barcode,ui_record_type,source_ip_address,source_country,browser_language)
                     SELECT unnest(array[%s]::numeric[])as ui_id,
                            unnest(array[%s]::numeric[]) as ui_user_id,
                            unnest(array['anji%s']::character varying[]) as ui_firstname,
                            unnest(array['batchu%s']::character varying[]) as ui_lastname,
                            unnest(array['1-1%s modumudi avg']::character varying[]) as ui_address1,
                            unnest(array['']::character varying[]) as ui_address2,
                            unnest(array['52112%s' ]::character varying[]) as ui_postcode,
                            unnest(array[461471]::integer[]) as ui_country_id,
							unnest(array['machilipatnam%s']::character varying[]) as ui_city,
							unnest(array['87328%s']::character varying[]) as ui_state_id,
							unnest(array['']::character varying[]) as ui_province,
							unnest(array['2016-09-01 07:07:49.738']::timestamp without time zone[]) as ui_last_modified,
							unnest(array['461407195%s']::character varying[]) as ui_barcode,
							unnest(array['S']::character varying[]) as ui_record_type,
							unnest(array['69.121.190.97']::inet[]) as source_ip_address,
							unnest(array['India']::text[]) as source_country,
							unnest(array['en-US']::text[]) as browser_language'''%(i,i,i,i,i,i,i,i,i))
                         
conn.commit()
print "SUCCESSFULLY INSERTED AND UPDATED THE TABLE USERS INFO"
