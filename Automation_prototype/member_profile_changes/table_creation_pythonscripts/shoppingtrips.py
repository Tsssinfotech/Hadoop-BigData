#importing the required packages
import os
import sys
import psycopg2
import time
import contextlib
import urllib
import ConfigParser

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   partition_range = config.get('Properties','partition_range')

####################################################################################################################################
conn= psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened bigdata database successfully"
cur = conn.cursor()
#DROP THE TABLE shoppingtrips
cur.execute('''drop table shoppingtrips;''')
conn.commit()
print "successfully drop the table shoppingtrips"
#CREATE A TABLE promotions

cur.execute('''CREATE TABLE IF NOT EXISTS shoppingtrips (dw_inserted_ts timestamp with time zone,dw_inserted_ts_utc timestamp with time zone,id bigint,
reward_id bigint,store_id bigint,member_id bigint,click_date character varying,tt_date_id character varying,click_time character varying,entity_id bigint,
entity_type_id character varying,coupon_id character varying,campaign_id character varying,tt_created_ts character varying,tt_modified_ts character varying,
tt_db_created_ts character varying,tt_db_modified_ts character varying,tracking_ticket_number character varying,tt_source_id character varying,
tt_source_name character varying,group_entity_id character varying,group_entity_type_id character varying,tracking_uri character varying,
shopping_uri character varying,non_cashback_member_id character varying,note character varying,nav_id character varying,ee_id character varying,
mpl_id character varying,referral_nav_id character varying,mst_db_created_ts character varying,mst_ods_inserted_ts character varying,
session_id character varying,event_id character varying,non_member_id character varying,hash_non_member_id character varying,
ip_address character varying,visit_session_id character varying,hash_session_id character varying,visit_id character varying,
referrer_id character varying,request_url_stem character varying,request_url_query character varying,browseragent character varying,browser_lang character varying,
referring_query character varying,referring_url character varying,is_signup character varying,is_login character varying,landing_url_domain character varying,
referring_url_domain character varying,ip_location_id character varying,country_short character varying,country_long character varying,region character varying,
city character varying,zipcode character varying,os_browser_id character varying,device_family character varying,os_family character varying,
os_version_int character varying,browser_family character varying,browser_version_int character varying,ua_parser_bot_flag character varying,
is_mobile character varying,is_tablet character varying,is_pc character varying,traffic_source_id character varying,traffic_source character varying,
traffic_source_type character varying,traffic_source_subtype character varying,application_id character varying,application_type character varying,
application_subtype character varying,visit_src_param character varying,visit_acct_param character varying,visit_campaign_param character varying,
visit_utype_param character varying,visit_product_param character varying,visit_gglnum_param character varying,visit_referrerid_param character varying,
visit_kw_param character varying,visit_adg_param character varying,visit_mt_param character varying,visit_query_param character varying,
is_fast_click_bit character varying,is_user_agent_bot character varying,merchant_rebate character varying,device_platform character varying,
parent_campaign_id character varying,cbsp_session_id character varying,is_cashback character varying,tracking_uri_domain character varying,
utm_medium character varying,utm_campaign character varying,utm_source character varying,utm_content character varying,utm_term character varying,
utm_size character varying,utm_pub character varying,utm_ebs character varying,url_format_2016 character varying,
mobile_app_session_id character varying,mobile_app_application_type character varying,mobile_app_application_subtype character varying,
mobile_device_platform character varying,mobile_app_version character varying,mobile_app_os_version character varying,mobile_app_eeid character varying,
apptimize_data character varying,mobile_app_device_ids character varying,mobile_app_android_id character varying);''')
conn.commit()
print "promotions table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE shoppingtrips"
for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*int(partition_range)
      for i in [x]:
             cur.execute('''INSERT INTO shoppingtrips(dw_inserted_ts,dw_inserted_ts_utc,id,reward_id,store_id,member_id,click_date,tt_date_id,click_time,entity_id,entity_type_id,
      coupon_id,campaign_id,tt_created_ts,tt_modified_ts,tt_db_created_ts,tt_db_modified_ts,tracking_ticket_number,tt_source_id,tt_source_name,group_entity_id, 
group_entity_type_id,tracking_uri,shopping_uri,non_cashback_member_id,note,nav_id,ee_id,mpl_id,referral_nav_id,mst_db_created_ts,mst_ods_inserted_ts,session_id, 
event_id,non_member_id,hash_non_member_id,ip_address,visit_session_id,hash_session_id,visit_id,referrer_id,request_url_stem,request_url_query,browseragent, 
browser_lang,referring_query,referring_url,is_signup,is_login,landing_url_domain,referring_url_domain,ip_location_id,country_short,country_long,region,city,
 zipcode,os_browser_id,device_family,os_family,os_version_int,browser_family,browser_version_int,ua_parser_bot_flag,is_mobile,is_tablet,is_pc,traffic_source_id, 
traffic_source,traffic_source_type,traffic_source_subtype,application_id,application_type,application_subtype,visit_src_param,visit_acct_param,visit_campaign_param, 
visit_utype_param,visit_product_param,visit_gglnum_param,visit_referrerid_param,visit_kw_param,visit_adg_param,visit_mt_param,visit_query_param,is_fast_click_bit, 
is_user_agent_bot,merchant_rebate,device_platform,parent_campaign_id,cbsp_session_id,is_cashback,tracking_uri_domain,utm_medium,utm_campaign,utm_source,utm_content, 
utm_term,utm_size,utm_pub,utm_ebs,url_format_2016,mobile_app_session_id,mobile_app_application_type,mobile_app_application_subtype,mobile_device_platform, 
mobile_app_version,mobile_app_os_version,mobile_app_eeid,apptimize_data,mobile_app_device_ids,mobile_app_android_id)
                     SELECT unnest(array['2016-10-13 04:38']::timestamp with time zone[])as dw_inserted_ts,
                            unnest(array['2016-10-13 11:38:42.548593+00:00']::timestamp with time zone[]) as dw_inserted_ts_utc,
                            unnest(array[%s]::bigint[]) as id,
                            unnest(array[12%s]::bigint[]) as reward_id,
                            unnest(array[82%s]::bigint[]) as store_id,
                            unnest(array[%s]::bigint[]) as member_id,
                            unnest(array['09/10/16 14:34']::character varying[]) as click_date,
                            unnest(array['20161009']::character varying[]) as tt_date_id,
                            unnest(array['14:34:12']::character varying[]) as click_time,
                            unnest(array[82%s]::bigint[])as entity_id,
                            unnest(array['1']::character varying[]) as entity_type_id,
                            unnest(array['None']::character varying[]) as coupon_id,
                            unnest(array['None']::character varying[]) as campaign_id,
                            unnest(array['09/10/16 14:34']::character varying[]) as tt_created_ts,
                            unnest(array['09/10/16 14:34']:: character varying[]) as tt_modified_ts,
                            unnest(array['09/10/16 14:34']::character varying[])as tt_db_created_ts,
                            unnest(array['09/10/16 14:34']::character varying[]) as tt_db_modified_ts,
                            unnest(array['1037355%s']::character varying[]) as tracking_ticket_number,
                            unnest(array['22392']::character varying[]) as tt_source_id,
                            unnest(array['Android-Phone']::character varying[]) as tt_source_name,
                            unnest(array['\N']::character varying[]) as group_entity_id, 
                            unnest(array[ '\N']::character varying[]) as group_entity_type_id,
                            unnest(array['http://click.linksynergy.com/fs-bin/click?id=AysPbYF8vuM&offerid=277852.10000533&subid=0&type=4&u1=%s']::character varying[]) as tracking_uri,
                            unnest(array['/shopping/ticket/mall_ctrl.do?merchant_id=8257']::character varying[]) as non_cashback_member_id,
                            unnest(array['\N']::character varying[])as note,
                            unnest(array['\N']::character varying[]) as nav_id,
                            unnest(array['None']::character varying[]) as ee_id,
                            unnest(array['None']::character varying[]) as mpl_id,
                            unnest(array['None']::character varying[]) as referral_nav_id,
                            unnest(array['None']:: character varying[]) as mst_db_created_ts,
                            unnest(array['None']::character varying[])as mst_ods_inserted_ts,
                            unnest(array['None']::character varying[]) as session_id, 
                            unnest(array['aaaC1xjo5JrObcT%s']::character varying[]) as session_id, 
                            unnest(array['None']::character varying[]) as event_id,
                            unnest(array['None']::character varying[]) as non_member_id,
                            unnest(array['None']::character varying[]) as hash_non_member_id,
                            unnest(array['None']::character varying[]) as ip_address,
                            unnest(array['None']::character varying[]) as visit_session_id,
                            unnest(array['None']::character varying[]) as hash_session_id,
                            unnest(array['None']::character varying[])as visit_id,
                            unnest(array['None']::character varying[]) as referrer_id,
                            unnest(array['None']::character varying[]) as request_url_stem,
                            unnest(array['None']::character varying[]) as request_url_query,
                            unnest(array['None']::character varying[]) as browseragent, 
                            unnest(array['None']:: character varying[]) as browser_lang,
                            unnest(array['None']::character varying[])as referring_query,
                            unnest(array['None']::character varying[]) as referring_url,
                            unnest(array['0']::character varying[]) as is_signup,
                            unnest(array['0']::character varying[]) as is_login,
                            unnest(array['None']::character varying[]) as landing_url_domain,
                            unnest(array['None']::character varying[]) as referring_url_domain,
                            unnest(array['N/A']::character varying[]) as ip_location_id,
                            unnest(array['N/A']::character varying[]) as country_short,
                            unnest(array['N/A']::character varying[]) as country_long,
                            unnest(array['N/A']::character varying[])as region,
                            unnest(array['N/A']::character varying[]) as city,
                            unnest(array['N/A']::character varying[]) as zipcode,
                            unnest(array['Android-Phone_Android_N/A_Android_N/A_False_True_False_False']::character varying[]) as os_browser_id,
                            unnest(array['Android-Phone']::character varying[]) as device_family,
                            unnest(array['Android']:: character varying[]) as os_family,
                            unnest(array['N/A']::character varying[]) as os_version_int,
                            unnest(array['Android']::character varying[]) as browser_family,
                            unnest(array['N/A']::character varying[])as browser_version_int,
                            unnest(array['FALSE']::character varying[]) as ua_parser_bot_flag,
                            unnest(array['TRUE']::character varying[]) as is_mobile,
                            unnest(array['FALSE']::character varying[]) as is_tablet,
                            unnest(array['FALSE']::character varying[]) as is_pc,
                            unnest(array['MobileApp_MobileApp_AndroidPhoneApp']:: character varying[]) as traffic_source_id, 
                            unnest(array['Mobile App']::character varying[])as traffic_source,
                            unnest(array['Mobile App']::character varying[]) as traffic_source_type,
                            unnest(array['Android Phone App']::character varying[]) as traffic_source_subtype,
                            unnest(array['App_Android_Phone']:: character varying[]) as application_id,
                            unnest(array['App']::character varying[]) as application_type,
                            unnest(array['Android Phone']::character varying[]) as application_subtype,
                            unnest(array['None']::character varying[]) as visit_src_param,
                            unnest(array['None']::character varying[]) as visit_acct_param,
                            unnest(array['None']::character varying[]) as visit_campaign_param, 
                            unnest(array['None']::character varying[])as visit_utype_param,
                            unnest(array['None']::character varying[]) as visit_product_param,
                            unnest(array['None']::character varying[]) as visit_gglnum_param,
                            unnest(array['None']::character varying[]) as visit_referrerid_param,
                            unnest(array['None']::character varying[]) as visit_kw_param,
                            unnest(array['None']:: character varying[]) as visit_adg_param,
                            unnest(array['None']::character varying[]) as visit_mt_param,
                            unnest(array['None']::character varying[]) as visit_query_param,
                            unnest(array['FALSE']::character varying[]) as is_fast_click_bit, 
                            unnest(array['FALSE']:: character varying[]) as is_user_agent_bot,
                            unnest(array['0']::character varying[]) as merchant_rebate,
                            unnest(array['Smartphone']::character varying[]) as device_platform,
                            unnest(array['-1']::character varying[])as parent_campaign_id,
                            unnest(array['\N']::character varying[]) as cbsp_session_id,
                            unnest(array['-1']::character varying[]) as is_cashback,
                            unnest(array['\N']::character varying[]) as tracking_uri_domain,
                            unnest(array['None']::character varying[]) as utm_medium,
                            unnest(array['None']:: character varying[]) as utm_campaign,
                            unnest(array['None']::character varying[])as utm_source,
                            unnest(array['none']::character varying[]) as utm_content, 
                            unnest(array['none']::character varying[]) as utm_term,
                            unnest(array['none']::character varying[]) as utm_size,
                            unnest(array['none']::character varying[]) as utm_pub,
                            unnest(array['none']::character varying[]) as utm_ebs,
                            unnest(array['none']::character varying[]) as url_format_2016,
                            unnest(array['none']::character varying[]) as mobile_app_session_id,
                            unnest(array['none']::character varying[]) as mobile_app_application_type,
                            unnest(array['none']::character varying[])as mobile_app_application_subtype,
                            unnest(array['none']::character varying[]) as mobile_device_platform, 
                            unnest(array['None']::character varying[]) as mobile_app_version,
                            unnest(array['None']::character varying[]) as mobile_app_os_version,
                            unnest(array['None']::character varying[]) as mobile_app_eeid,
                            unnest(array['None']:: character varying[]) as apptimize_data,
                            unnest(array['None']::character varying[]) as mobile_app_device_ids,
                            unnest(array['None']:: character varying[]) as mobile_app_android_id'''%(i,i,i,i,i,i,i,i))
conn.commit()
print "SUCCESSFULLY INSERTED AND UPDATED THE TABLE SHOPPINGTRIPS"
