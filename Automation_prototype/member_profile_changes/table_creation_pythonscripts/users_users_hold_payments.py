#importing the required packages
import os
import sys
import psycopg2
import time
import contextlib
import ConfigParser
#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   partition_range = config.get('Properties','partition_range')

####################################################################################################################################
@contextlib.contextmanager
def timer(name="duration"):
    'Utility function for timing execution'
    start=time.time()
    yield
    duration=time.time()-start
    print "{0}: {1} second(s)".format(name,duration)

conn= psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened bigdata database successfully"
cur = conn.cursor()
#DROP THE TABLE users_user_hold_payment
cur.execute('''drop table users_user_hold_payment;''')
conn.commit()
print "successfully drop the table users_user_hold_payment"
#CREATE A TABLE users_user_hold_payment

cur.execute('''create table if not exists users_user_hold_payment(id integer,user_id numeric,hold_payment boolean,modified_ts timestamp with time zone);''')
conn.commit()
print "users_user_hold_payment table created successfully in database bigdata with owner postgres"


#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE users_user_hold_payment"
for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*int(partition_range)
      for i in [x]:
             cur.execute('''INSERT INTO users_user_hold_payment(id,user_id,hold_payment,modified_ts)
                     SELECT unnest(array[%s]::integer[])as id,
                            unnest(array[%s]::numeric[]) as user_id,
                            unnest(array['False']::boolean[]) as hold_payment,
							unnest(array['2015-01-31 20:57:41.922000-08:00']::timestamp with time zone[]) as modified_ts'''%(i,i))
                         
conn.commit()
print "SUCCESSFULLY INSERTED AND UPDATED THE TABLE USERS_USER_HOLD_PAYMENT"
