#!/bin/bash
. /home/engineering/member_profile/config.properties
ods_inserted_ts=$(TZ='America/Los_Angeles' date +'%Y-%m-%d %H:%M:%S.%6N-07:00')
echo $ods_inserted_ts
ods_inserted_ts_utc=$(TZ='Etc/UTC' date +'%Y-%m-%d %H:%M:%S.%6N+00:00')
echo $ods_inserted_ts_utc

sqoop job --create  users_info -- import -m 1  --connect "$db_admin_url" --username postgres --password postgres123  --query "select'$ods_inserted_ts'::text,'$ods_inserted_ts_utc'::text as utc,ui_id,ui_user_id,ui_firstname,ui_lastname,ui_address1,ui_address2,ui_postcode,ui_country_id,ui_city,ui_state_id,ui_province,ui_last_modified,ui_barcode,ui_record_type,source_ip_address::text,source_country,browser_language from users_info where  \$CONDITIONS"  --incremental append -check-column ui_id --fields-terminated-by "\t"  --hive-delims-replacement "anything"  --null-string "\\\\N" --null-non-string "\\\\N"   --hive-table users_info --target-dir $hdfs_base_dir/temp/dealwallet.com/public_new_users_info -- --schema public
