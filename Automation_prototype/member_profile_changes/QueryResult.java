// ORM class for table 'null'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Sat Feb 18 22:17:31 PST 2017
// For connector: org.apache.sqoop.manager.PostgresqlManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class QueryResult extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  public static interface FieldSetterCommand {    void setField(Object value);  }  protected ResultSet __cur_result_set;
  private Map<String, FieldSetterCommand> setters = new HashMap<String, FieldSetterCommand>();
  private void init0() {
    setters.put("text", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        text = (String)value;
      }
    });
    setters.put("utc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        utc = (String)value;
      }
    });
    setters.put("upo_user_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_user_id = (Integer)value;
      }
    });
    setters.put("upo_payment_group_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_payment_group_id = (Integer)value;
      }
    });
    setters.put("upo_payment_method_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_payment_method_id = (Integer)value;
      }
    });
    setters.put("upo_third_party", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_third_party = (Boolean)value;
      }
    });
    setters.put("upo_payment_email_address", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_payment_email_address = (String)value;
      }
    });
    setters.put("upo_last_modified", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_last_modified = (java.sql.Timestamp)value;
      }
    });
    setters.put("source_ip_address", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        source_ip_address = (String)value;
      }
    });
    setters.put("source_country", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        source_country = (String)value;
      }
    });
    setters.put("upo_yandex_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        upo_yandex_id = (String)value;
      }
    });
  }
  public QueryResult() {
    init0();
  }
  private String text;
  public String get_text() {
    return text;
  }
  public void set_text(String text) {
    this.text = text;
  }
  public QueryResult with_text(String text) {
    this.text = text;
    return this;
  }
  private String utc;
  public String get_utc() {
    return utc;
  }
  public void set_utc(String utc) {
    this.utc = utc;
  }
  public QueryResult with_utc(String utc) {
    this.utc = utc;
    return this;
  }
  private Integer upo_user_id;
  public Integer get_upo_user_id() {
    return upo_user_id;
  }
  public void set_upo_user_id(Integer upo_user_id) {
    this.upo_user_id = upo_user_id;
  }
  public QueryResult with_upo_user_id(Integer upo_user_id) {
    this.upo_user_id = upo_user_id;
    return this;
  }
  private Integer upo_payment_group_id;
  public Integer get_upo_payment_group_id() {
    return upo_payment_group_id;
  }
  public void set_upo_payment_group_id(Integer upo_payment_group_id) {
    this.upo_payment_group_id = upo_payment_group_id;
  }
  public QueryResult with_upo_payment_group_id(Integer upo_payment_group_id) {
    this.upo_payment_group_id = upo_payment_group_id;
    return this;
  }
  private Integer upo_payment_method_id;
  public Integer get_upo_payment_method_id() {
    return upo_payment_method_id;
  }
  public void set_upo_payment_method_id(Integer upo_payment_method_id) {
    this.upo_payment_method_id = upo_payment_method_id;
  }
  public QueryResult with_upo_payment_method_id(Integer upo_payment_method_id) {
    this.upo_payment_method_id = upo_payment_method_id;
    return this;
  }
  private Boolean upo_third_party;
  public Boolean get_upo_third_party() {
    return upo_third_party;
  }
  public void set_upo_third_party(Boolean upo_third_party) {
    this.upo_third_party = upo_third_party;
  }
  public QueryResult with_upo_third_party(Boolean upo_third_party) {
    this.upo_third_party = upo_third_party;
    return this;
  }
  private String upo_payment_email_address;
  public String get_upo_payment_email_address() {
    return upo_payment_email_address;
  }
  public void set_upo_payment_email_address(String upo_payment_email_address) {
    this.upo_payment_email_address = upo_payment_email_address;
  }
  public QueryResult with_upo_payment_email_address(String upo_payment_email_address) {
    this.upo_payment_email_address = upo_payment_email_address;
    return this;
  }
  private java.sql.Timestamp upo_last_modified;
  public java.sql.Timestamp get_upo_last_modified() {
    return upo_last_modified;
  }
  public void set_upo_last_modified(java.sql.Timestamp upo_last_modified) {
    this.upo_last_modified = upo_last_modified;
  }
  public QueryResult with_upo_last_modified(java.sql.Timestamp upo_last_modified) {
    this.upo_last_modified = upo_last_modified;
    return this;
  }
  private String source_ip_address;
  public String get_source_ip_address() {
    return source_ip_address;
  }
  public void set_source_ip_address(String source_ip_address) {
    this.source_ip_address = source_ip_address;
  }
  public QueryResult with_source_ip_address(String source_ip_address) {
    this.source_ip_address = source_ip_address;
    return this;
  }
  private String source_country;
  public String get_source_country() {
    return source_country;
  }
  public void set_source_country(String source_country) {
    this.source_country = source_country;
  }
  public QueryResult with_source_country(String source_country) {
    this.source_country = source_country;
    return this;
  }
  private String upo_yandex_id;
  public String get_upo_yandex_id() {
    return upo_yandex_id;
  }
  public void set_upo_yandex_id(String upo_yandex_id) {
    this.upo_yandex_id = upo_yandex_id;
  }
  public QueryResult with_upo_yandex_id(String upo_yandex_id) {
    this.upo_yandex_id = upo_yandex_id;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.text == null ? that.text == null : this.text.equals(that.text));
    equal = equal && (this.utc == null ? that.utc == null : this.utc.equals(that.utc));
    equal = equal && (this.upo_user_id == null ? that.upo_user_id == null : this.upo_user_id.equals(that.upo_user_id));
    equal = equal && (this.upo_payment_group_id == null ? that.upo_payment_group_id == null : this.upo_payment_group_id.equals(that.upo_payment_group_id));
    equal = equal && (this.upo_payment_method_id == null ? that.upo_payment_method_id == null : this.upo_payment_method_id.equals(that.upo_payment_method_id));
    equal = equal && (this.upo_third_party == null ? that.upo_third_party == null : this.upo_third_party.equals(that.upo_third_party));
    equal = equal && (this.upo_payment_email_address == null ? that.upo_payment_email_address == null : this.upo_payment_email_address.equals(that.upo_payment_email_address));
    equal = equal && (this.upo_last_modified == null ? that.upo_last_modified == null : this.upo_last_modified.equals(that.upo_last_modified));
    equal = equal && (this.source_ip_address == null ? that.source_ip_address == null : this.source_ip_address.equals(that.source_ip_address));
    equal = equal && (this.source_country == null ? that.source_country == null : this.source_country.equals(that.source_country));
    equal = equal && (this.upo_yandex_id == null ? that.upo_yandex_id == null : this.upo_yandex_id.equals(that.upo_yandex_id));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.text == null ? that.text == null : this.text.equals(that.text));
    equal = equal && (this.utc == null ? that.utc == null : this.utc.equals(that.utc));
    equal = equal && (this.upo_user_id == null ? that.upo_user_id == null : this.upo_user_id.equals(that.upo_user_id));
    equal = equal && (this.upo_payment_group_id == null ? that.upo_payment_group_id == null : this.upo_payment_group_id.equals(that.upo_payment_group_id));
    equal = equal && (this.upo_payment_method_id == null ? that.upo_payment_method_id == null : this.upo_payment_method_id.equals(that.upo_payment_method_id));
    equal = equal && (this.upo_third_party == null ? that.upo_third_party == null : this.upo_third_party.equals(that.upo_third_party));
    equal = equal && (this.upo_payment_email_address == null ? that.upo_payment_email_address == null : this.upo_payment_email_address.equals(that.upo_payment_email_address));
    equal = equal && (this.upo_last_modified == null ? that.upo_last_modified == null : this.upo_last_modified.equals(that.upo_last_modified));
    equal = equal && (this.source_ip_address == null ? that.source_ip_address == null : this.source_ip_address.equals(that.source_ip_address));
    equal = equal && (this.source_country == null ? that.source_country == null : this.source_country.equals(that.source_country));
    equal = equal && (this.upo_yandex_id == null ? that.upo_yandex_id == null : this.upo_yandex_id.equals(that.upo_yandex_id));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.text = JdbcWritableBridge.readString(1, __dbResults);
    this.utc = JdbcWritableBridge.readString(2, __dbResults);
    this.upo_user_id = JdbcWritableBridge.readInteger(3, __dbResults);
    this.upo_payment_group_id = JdbcWritableBridge.readInteger(4, __dbResults);
    this.upo_payment_method_id = JdbcWritableBridge.readInteger(5, __dbResults);
    this.upo_third_party = JdbcWritableBridge.readBoolean(6, __dbResults);
    this.upo_payment_email_address = JdbcWritableBridge.readString(7, __dbResults);
    this.upo_last_modified = JdbcWritableBridge.readTimestamp(8, __dbResults);
    this.source_ip_address = JdbcWritableBridge.readString(9, __dbResults);
    this.source_country = JdbcWritableBridge.readString(10, __dbResults);
    this.upo_yandex_id = JdbcWritableBridge.readString(11, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.text = JdbcWritableBridge.readString(1, __dbResults);
    this.utc = JdbcWritableBridge.readString(2, __dbResults);
    this.upo_user_id = JdbcWritableBridge.readInteger(3, __dbResults);
    this.upo_payment_group_id = JdbcWritableBridge.readInteger(4, __dbResults);
    this.upo_payment_method_id = JdbcWritableBridge.readInteger(5, __dbResults);
    this.upo_third_party = JdbcWritableBridge.readBoolean(6, __dbResults);
    this.upo_payment_email_address = JdbcWritableBridge.readString(7, __dbResults);
    this.upo_last_modified = JdbcWritableBridge.readTimestamp(8, __dbResults);
    this.source_ip_address = JdbcWritableBridge.readString(9, __dbResults);
    this.source_country = JdbcWritableBridge.readString(10, __dbResults);
    this.upo_yandex_id = JdbcWritableBridge.readString(11, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(text, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(utc, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_user_id, 3 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_payment_group_id, 4 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_payment_method_id, 5 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeBoolean(upo_third_party, 6 + __off, -7, __dbStmt);
    JdbcWritableBridge.writeString(upo_payment_email_address, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeTimestamp(upo_last_modified, 8 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(source_ip_address, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(source_country, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(upo_yandex_id, 11 + __off, 12, __dbStmt);
    return 11;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(text, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(utc, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_user_id, 3 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_payment_group_id, 4 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(upo_payment_method_id, 5 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeBoolean(upo_third_party, 6 + __off, -7, __dbStmt);
    JdbcWritableBridge.writeString(upo_payment_email_address, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeTimestamp(upo_last_modified, 8 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(source_ip_address, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(source_country, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(upo_yandex_id, 11 + __off, 12, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.text = null;
    } else {
    this.text = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.utc = null;
    } else {
    this.utc = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.upo_user_id = null;
    } else {
    this.upo_user_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.upo_payment_group_id = null;
    } else {
    this.upo_payment_group_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.upo_payment_method_id = null;
    } else {
    this.upo_payment_method_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.upo_third_party = null;
    } else {
    this.upo_third_party = Boolean.valueOf(__dataIn.readBoolean());
    }
    if (__dataIn.readBoolean()) { 
        this.upo_payment_email_address = null;
    } else {
    this.upo_payment_email_address = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.upo_last_modified = null;
    } else {
    this.upo_last_modified = new Timestamp(__dataIn.readLong());
    this.upo_last_modified.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.source_ip_address = null;
    } else {
    this.source_ip_address = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.source_country = null;
    } else {
    this.source_country = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.upo_yandex_id = null;
    } else {
    this.upo_yandex_id = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.text) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, text);
    }
    if (null == this.utc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, utc);
    }
    if (null == this.upo_user_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_user_id);
    }
    if (null == this.upo_payment_group_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_payment_group_id);
    }
    if (null == this.upo_payment_method_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_payment_method_id);
    }
    if (null == this.upo_third_party) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeBoolean(this.upo_third_party);
    }
    if (null == this.upo_payment_email_address) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, upo_payment_email_address);
    }
    if (null == this.upo_last_modified) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.upo_last_modified.getTime());
    __dataOut.writeInt(this.upo_last_modified.getNanos());
    }
    if (null == this.source_ip_address) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, source_ip_address);
    }
    if (null == this.source_country) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, source_country);
    }
    if (null == this.upo_yandex_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, upo_yandex_id);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.text) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, text);
    }
    if (null == this.utc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, utc);
    }
    if (null == this.upo_user_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_user_id);
    }
    if (null == this.upo_payment_group_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_payment_group_id);
    }
    if (null == this.upo_payment_method_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.upo_payment_method_id);
    }
    if (null == this.upo_third_party) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeBoolean(this.upo_third_party);
    }
    if (null == this.upo_payment_email_address) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, upo_payment_email_address);
    }
    if (null == this.upo_last_modified) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.upo_last_modified.getTime());
    __dataOut.writeInt(this.upo_last_modified.getNanos());
    }
    if (null == this.source_ip_address) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, source_ip_address);
    }
    if (null == this.source_country) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, source_country);
    }
    if (null == this.upo_yandex_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, upo_yandex_id);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 9, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(text==null?"\\N":text, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(utc==null?"\\N":utc, "anything", delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_user_id==null?"\\N":"" + upo_user_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_payment_group_id==null?"\\N":"" + upo_payment_group_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_payment_method_id==null?"\\N":"" + upo_payment_method_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_third_party==null?"\\N":"" + upo_third_party, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(upo_payment_email_address==null?"\\N":upo_payment_email_address, "anything", delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_last_modified==null?"\\N":"" + upo_last_modified, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(source_ip_address==null?"\\N":source_ip_address, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(source_country==null?"\\N":source_country, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(upo_yandex_id==null?"\\N":upo_yandex_id, "anything", delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(text==null?"\\N":text, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(utc==null?"\\N":utc, "anything", delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_user_id==null?"\\N":"" + upo_user_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_payment_group_id==null?"\\N":"" + upo_payment_group_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_payment_method_id==null?"\\N":"" + upo_payment_method_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_third_party==null?"\\N":"" + upo_third_party, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(upo_payment_email_address==null?"\\N":upo_payment_email_address, "anything", delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(upo_last_modified==null?"\\N":"" + upo_last_modified, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(source_ip_address==null?"\\N":source_ip_address, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(source_country==null?"\\N":source_country, "anything", delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, replacing delimiters \n,\r,\01 with 'anything' from strings
    __sb.append(FieldFormatter.hiveStringReplaceDelims(upo_yandex_id==null?"\\N":upo_yandex_id, "anything", delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 9, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.text = null; } else {
      this.text = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.utc = null; } else {
      this.utc = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_user_id = null; } else {
      this.upo_user_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_payment_group_id = null; } else {
      this.upo_payment_group_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_payment_method_id = null; } else {
      this.upo_payment_method_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_third_party = null; } else {
      this.upo_third_party = BooleanParser.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.upo_payment_email_address = null; } else {
      this.upo_payment_email_address = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_last_modified = null; } else {
      this.upo_last_modified = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.source_ip_address = null; } else {
      this.source_ip_address = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.source_country = null; } else {
      this.source_country = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.upo_yandex_id = null; } else {
      this.upo_yandex_id = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.text = null; } else {
      this.text = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.utc = null; } else {
      this.utc = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_user_id = null; } else {
      this.upo_user_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_payment_group_id = null; } else {
      this.upo_payment_group_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_payment_method_id = null; } else {
      this.upo_payment_method_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_third_party = null; } else {
      this.upo_third_party = BooleanParser.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.upo_payment_email_address = null; } else {
      this.upo_payment_email_address = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.upo_last_modified = null; } else {
      this.upo_last_modified = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.source_ip_address = null; } else {
      this.source_ip_address = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.source_country = null; } else {
      this.source_country = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.upo_yandex_id = null; } else {
      this.upo_yandex_id = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    QueryResult o = (QueryResult) super.clone();
    o.upo_last_modified = (o.upo_last_modified != null) ? (java.sql.Timestamp) o.upo_last_modified.clone() : null;
    return o;
  }

  public void clone0(QueryResult o) throws CloneNotSupportedException {
    o.upo_last_modified = (o.upo_last_modified != null) ? (java.sql.Timestamp) o.upo_last_modified.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new HashMap<String, Object>();
    __sqoop$field_map.put("text", this.text);
    __sqoop$field_map.put("utc", this.utc);
    __sqoop$field_map.put("upo_user_id", this.upo_user_id);
    __sqoop$field_map.put("upo_payment_group_id", this.upo_payment_group_id);
    __sqoop$field_map.put("upo_payment_method_id", this.upo_payment_method_id);
    __sqoop$field_map.put("upo_third_party", this.upo_third_party);
    __sqoop$field_map.put("upo_payment_email_address", this.upo_payment_email_address);
    __sqoop$field_map.put("upo_last_modified", this.upo_last_modified);
    __sqoop$field_map.put("source_ip_address", this.source_ip_address);
    __sqoop$field_map.put("source_country", this.source_country);
    __sqoop$field_map.put("upo_yandex_id", this.upo_yandex_id);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("text", this.text);
    __sqoop$field_map.put("utc", this.utc);
    __sqoop$field_map.put("upo_user_id", this.upo_user_id);
    __sqoop$field_map.put("upo_payment_group_id", this.upo_payment_group_id);
    __sqoop$field_map.put("upo_payment_method_id", this.upo_payment_method_id);
    __sqoop$field_map.put("upo_third_party", this.upo_third_party);
    __sqoop$field_map.put("upo_payment_email_address", this.upo_payment_email_address);
    __sqoop$field_map.put("upo_last_modified", this.upo_last_modified);
    __sqoop$field_map.put("source_ip_address", this.source_ip_address);
    __sqoop$field_map.put("source_country", this.source_country);
    __sqoop$field_map.put("upo_yandex_id", this.upo_yandex_id);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if (!setters.containsKey(__fieldName)) {
      throw new RuntimeException("No such field:"+__fieldName);
    }
    setters.get(__fieldName).setField(__fieldVal);
  }

}
